Simple Telegram bot providing bridges

You can contact it at @GetBridgesBot in telegram and ask it for bridges with `/bridges`.

The bot is run by passing to it a file containing the bridges lines and the Telegram bot token:
```
./telegram-bridge-bot -t TOKEN -f bridges
```

To prevent listing the bridges it will give always the same bridge to each disticnt account that request bridges for for 24 hours.
