package main

import (
	"fmt"
	"net/http"

	"github.com/prometheus/client_golang/prometheus"
	"github.com/prometheus/client_golang/prometheus/promauto"
	"github.com/prometheus/client_golang/prometheus/promhttp"
)

var (
	bridgeRequestsCount = promauto.NewCounterVec(prometheus.CounterOpts{
		Name: "telegram_bridges_request_total",
		Help: "The total number of bridge requests",
	},
		[]string{"bridge"},
	)
)

func SetupMetrics(port int) {
	http.Handle("/metrics", promhttp.Handler())
	go http.ListenAndServe(fmt.Sprintf(":%d", port), nil)
}
