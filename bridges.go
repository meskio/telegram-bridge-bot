package main

import (
	"bufio"
	"crypto/rand"
	"encoding/json"
	"log"
	"math/big"
	"os"
	"strconv"
	"time"

	"go.etcd.io/bbolt"
)

var (
	bucket = []byte("handed")
)

const (
	handedExpiration = time.Hour * 24
)

type Bridges struct {
	bridges      []string
	db           *bbolt.DB
	metricsLabel string
}

func parseBridges(path string, db *bbolt.DB, metricsLabel string) (*Bridges, error) {
	f, err := os.Open(path)
	if err != nil {
		return nil, err
	}
	defer f.Close()

	fileScanner := bufio.NewScanner(f)
	fileScanner.Split(bufio.ScanLines)
	var bridges []string
	for fileScanner.Scan() {
		bridges = append(bridges, fileScanner.Text())
	}

	err = db.Update(func(tx *bbolt.Tx) error {
		_, err := tx.CreateBucketIfNotExists(bucket)
		return err
	})
	if err != nil {
		return nil, err
	}

	return &Bridges{bridges, db, metricsLabel}, nil
}

func (b *Bridges) getBridge(userID int) string {
	handedLine := b.handedBridge(userID)
	if handedLine != "" {
		bridgeRequestsCount.WithLabelValues("cached").Inc()
		return handedLine
	}

	if len(b.bridges) == 0 {
		log.Println("No bridges available")
		return ""
	}

	max := big.NewInt(int64(len(b.bridges)))
	i, err := rand.Int(rand.Reader, max)
	if err != nil {
		bridgeRequestsCount.WithLabelValues("error").Inc()
		log.Println("Error getting random index for bridges:", err)
		return ""
	}

	bridgeLine := b.bridges[i.Int64()]
	b.addHandedBridge(userID, bridgeLine)
	bridgeRequestsCount.WithLabelValues(b.metricsLabel).Inc()
	return bridgeLine
}

type handedBridge struct {
	BridgeLine   string
	CreationDate time.Time
}

func (b *Bridges) handedBridge(userID int) string {
	var encodedValue []byte
	b.db.View(func(tx *bbolt.Tx) error {
		b := tx.Bucket(bucket)
		encodedValue = b.Get(userIDtoKey(userID))
		return nil
	})

	if encodedValue == nil {
		return ""
	}

	var handed handedBridge
	err := json.Unmarshal(encodedValue, &handed)
	if err != nil {
		log.Println("Error unmarshaling from db:", err)
		return ""
	}

	if handed.CreationDate.Add(handedExpiration).Before(time.Now()) {
		return ""
	}

	return handed.BridgeLine
}

func (b *Bridges) addHandedBridge(userID int, bridgeLine string) {
	handed := handedBridge{
		BridgeLine:   bridgeLine,
		CreationDate: time.Now(),
	}

	encodedValue, err := json.Marshal(handed)
	if err != nil {
		log.Println("Error marshaling haded bridge:", err)
		return
	}

	err = b.db.Update(func(tx *bbolt.Tx) error {
		b := tx.Bucket(bucket)
		return b.Put([]byte(userIDtoKey(userID)), encodedValue)
	})
	if err != nil {
		log.Println("Error adding handed bridge to DB:", err)
		return
	}

}

func userIDtoKey(userID int) []byte {
	return []byte(strconv.Itoa(userID))
}
