package main

import (
	"time"

	tb "gopkg.in/tucnak/telebot.v2"
)

const (
	TelegramPollTimeout = 10 * time.Second
)

type TBot struct {
	bot     *tb.Bot
	bridges []*Bridges
	userIDs []int
}

func newTBot(token string, userIDs []int, bridges []*Bridges) (t *TBot, err error) {
	t = new(TBot)
	t.bridges = bridges
	t.userIDs = userIDs

	t.bot, err = tb.NewBot(tb.Settings{
		Token:  token,
		Poller: &tb.LongPoller{Timeout: TelegramPollTimeout},
	})

	if err != nil {
		return
	}

	t.bot.Handle("/bridges", t.getBridges)
	return
}

func (t *TBot) Start() {
	t.bot.Start()
}

func (t *TBot) getBridges(m *tb.Message) {
	if m.Sender.IsBot {
		t.bot.Send(m.Sender, "No bridges for bots, sorry")
		return
	}

	userID := m.Sender.ID
	bridges := t.bridges[len(t.bridges)-1]
	for i, id := range t.userIDs {
		if userID < id {
			bridges = t.bridges[i]
			break
		}
	}

	t.bot.Send(m.Sender, bridges.getBridge(userID))
}
