package main

import (
	"flag"
	"fmt"
	"log"
	"os"

	"go.etcd.io/bbolt"
)

func main() {
	token := flag.String("t", "", "Telegram token")
	bridgeFile := flag.String("f", "", "Bridge file")
	bridgeFile1 := flag.String("f1", "", "Bridge file for new")
	bridgeFile2 := flag.String("f2", "", "Bridge file for newer accounts")
	dbFile := flag.String("db", "bridges.db", "DB file")
	metricsPort := flag.Int("mp", 2112, "Metrics port")
	userID1 := flag.Int("id1", 0, "Minimum user ID for new accounts")
	userID2 := flag.Int("id2", 0, "Minimum user ID for newer accounts")
	flag.Parse()

	SetupMetrics(*metricsPort)

	if *token == "" || *bridgeFile == "" {
		log.Fatal(usage())
	}
	db, err := bbolt.Open(*dbFile, 0660, nil)
	if err != nil {
		log.Fatal("Error opening db", err)
	}

	bridges := []*Bridges{}
	b, err := parseBridges(*bridgeFile, db, "old")
	if err != nil {
		log.Fatal("Error loading bridge file:", err)
	}
	bridges = append(bridges, b)

	b, err = parseBridges(*bridgeFile1, db, "new")
	if err != nil {
		log.Fatal("Error loading bridge file1:", err)
	}
	bridges = append(bridges, b)

	b, err = parseBridges(*bridgeFile2, db, "newer")
	if err != nil {
		log.Fatal("Error loading bridge file2:", err)
	}
	bridges = append(bridges, b)

	tbot, err := newTBot(*token, []int{*userID1, *userID2}, bridges)
	if err != nil {
		log.Fatal(err)
	}
	tbot.Start()
}

func usage() string {
	return fmt.Sprintf("%s -t TELEGRAM_TOKEN -f path/bridges -db dbfile", os.Args[0])
}
