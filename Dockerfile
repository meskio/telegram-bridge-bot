FROM golang:1.17 AS builder

COPY *.go go.* /bot/

WORKDIR /bot
RUN go mod download
RUN CGO_ENABLED=0 go build -o bot -ldflags '-extldflags "-static"'  .

FROM scratch

COPY --from=builder /etc/ssl/certs/ca-certificates.crt /etc/ssl/certs/ca-certificates.crt
COPY --from=builder /usr/share/zoneinfo /usr/share/zoneinfo
COPY --from=builder /bot/bot /bin/bot

ENTRYPOINT [ "/bin/bot" ]
